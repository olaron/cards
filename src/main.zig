const std = @import("std");
const print = std.debug.print;
const DefaultPrng = std.rand.DefaultPrng;

var prng = DefaultPrng.init(1);
const rnd = prng.random();

const NB_OF_CARDS: u8 = 100;
const DECK_SIZE: u8 = 30;
const MAX_HAND_SIZE: u8 = 5;
const START_LIFE: i16 = 20;


const atk_names: [20][]const u8 = [_][]const u8{"Fee", "Fee", "Kobold", "Kobold", "Kobold", "Gobelin", "Gobelin", "Gobelin", "Gobelin", "Orc", "Orc", "Orc", "Orc", "Orc", "Troll", "Troll", "Troll", "Ogre", "Ogre", "Dragon"};
const def_names: [20][]const u8 = [_][]const u8{"papier", "mousseline", "cire", "faience", "cristal", "corail", "perle", "bois", "fer", "cuivre", "bronze", "argent", "or", "platine", "titane", "topaze", "rubis", "zephir", "onyx", "diamant"};

fn cmp(_: void, a: u8, b: u8) bool
{
    return a < b;
}

const Card = struct {
    cost: u8,
    attack: u8,
    defense: u8,

    pub fn print_card(s: Card) void
    {
        print("({}:{}/{}) {s} de {s}\n",.{s.cost, s.attack, s.defense, atk_names[s.attack], def_names[s.defense - 1]});
    }
};

const all_cards = init: {
    var initial_value: [NB_OF_CARDS]Card = undefined;
    var cost = 1;
    var defense = 2 * cost;
    var attack = 0;
    for (initial_value) |*c| {
        c.* = Card{
            .cost = cost,
            .attack = attack,
            .defense = defense,
        };
        defense -= 1;
        attack += 1;
        if(defense == 0)
        {
            cost += 1;
            defense = 2 * cost;
            attack = 0;
        }
    }
    break :init initial_value;
};


const Player = struct {
    life: i16 = START_LIFE,
    mana: u8 = 0,
    mana_max: u8 = 0,
    deck: [DECK_SIZE]u8,
    deck_size: u8 = DECK_SIZE,
    hand: [MAX_HAND_SIZE]i16 = [_]i16{-1} ** MAX_HAND_SIZE,
    hand_size: u8 = 0,
    board: [DECK_SIZE]i16 = [_]i16{-1} ** DECK_SIZE,
    mal: [DECK_SIZE]bool = [_]bool{true} ** DECK_SIZE,
    board_size: u8 = 0,

    pub fn new() Player {
        var d : [DECK_SIZE]u8 = init: {
            var deck: [DECK_SIZE]u8 = undefined;
            for (deck) |*c| {
                c.* = rnd.uintLessThan(u8, NB_OF_CARDS);
            }
            break :init deck;
        };

        var p = Player {
            .deck = d,
        };
        return p;
    }


    pub fn print_deck(s: Player) void {
        print("Deck:\n",.{});
        var d = s.deck;
        std.sort.sort(u8, d[0..], {}, cmp);
        print("{d}\n",.{d});
        for(d) |*c|
        {
            print("{}: ",.{c.*});
            all_cards[c.*].print_card();
        }
    }

    pub fn mutate(s: *Player, mutations: u8) void
    {
        var i : u8 = 0;
        while(i < mutations)
        {
            var rpos = rnd.uintLessThan(u8, DECK_SIZE);
            var rcard = rnd.uintLessThan(u8, NB_OF_CARDS);
            s.deck[rpos] = rcard;
            i += 1;
        }
    }

    pub fn shuffle_deck(s: *Player) void {
        var i : u8 = 0;
        while(i < DECK_SIZE)
        {
            var swap_pos = rnd.uintLessThan(u8, DECK_SIZE);
            if (swap_pos != i)
            {
                var tmp = s.deck[i];
                s.deck[i] = s.deck[swap_pos];
                s.deck[swap_pos] = tmp;
            }
            i += 1;
        }
    }

    pub fn new_game(s: *Player) void {
        s.life = START_LIFE;
        s.mana = 0;
        s.mana_max = 0;
        s.deck_size = DECK_SIZE;
        s.hand = [_]i16{-1} ** MAX_HAND_SIZE;
        s.hand_size = 0;
        s.board = [_]i16{-1} ** DECK_SIZE;
        s.mal = [_]bool{true} ** DECK_SIZE;
        s.board_size = 0;

        s.shuffle_deck();

        var i : u8 = 0;
        while(i < MAX_HAND_SIZE)
        {
            s.draw();
            i += 1;
        }

    }

    pub fn new_turn(s: *Player) void {
        s.mana_max += 1;
        s.mana = s.mana_max;
    }

    pub fn draw(s: *Player) void {
        if(s.deck_size == 0)
        {
            s.life -= 1;
            return;
        }
        if(s.hand_size < MAX_HAND_SIZE)
        {
            for(s.hand) |*c|
            {
                if(c.* == -1)
                {
                    c.* = s.deck[s.deck_size -  1];
                    s.deck_size -= 1;
                    s.hand_size += 1;
                }
            }
        }
    }

    pub fn play_card(s: *Player) bool {
        var higher_cost_card: i16 = -1;
        var cost: u8 = 0;
        var position_in_hand: usize = 0;
        for(s.hand) |*c, i|
        {
            if(c.* > higher_cost_card)
            {
                var co: u8 = all_cards[@intCast(usize,c.*)].cost;
                if(co <= s.mana)
                {
                    higher_cost_card = c.*;
                    position_in_hand = i;
                    cost = co;

                }
            }
        }
        if(higher_cost_card > -1)
        {
            s.board[s.board_size] = higher_cost_card;
            s.hand[position_in_hand] = -1;
            s.hand_size -= 1;
            s.board_size += 1;
            s.mana -= cost;
            return true;
        }
        return false;
    }

    pub fn fight(s: *Player, o: *Player) void
    {
        var i: usize = 0;
        while(i < s.board_size)
        {
            var card = s.board[i];
            if(card > -1)
            {
                if(s.mal[i])
                {
                    s.mal[i] = false;
                }
                else
                {
                    o.life -= all_cards[@intCast(usize,card)].attack;
                }
                i+=1;
            }
        }
    }

    pub fn turn(s: *Player, o: *Player) void {
        s.new_turn();
        s.draw();
        while(s.play_card()){}
        s.fight(o);
    }
};

pub fn match(p1: *Player, p2: *Player) u8 {
    var i : u8 = 0;
    while((p1.life > 0) and (p2.life > 0))
    {
        i += 1;
        p1.turn(p2);
        if(p2.life <= 0)
        {
            break;
        }
        p2.turn(p1);
    }
    if(p1.life > 0)
    {
        return 1;
    }
    if(p2.life > 0)
    {
        return 2;
    }
    return 0;
}

const Result = struct {
    p1_wins: u32 = 0,
    p2_wins: u32 = 0,
    winner: u8 = 0,
};

pub fn multiple_matches(p1: *Player, p2: *Player, n: u32) Result {
    var i: u32 = 0;
    var result = Result{};
    while(i < n)
    {
        {
            p1.new_game();
            p2.new_game();
            var p_win = match(p1, p2);
            if(p_win == 1)
            {
                result.p1_wins += 1;
            }
            if(p_win == 2)
            {
                result.p2_wins += 1;
            }
            if(p_win == 0)
            {
                unreachable;
            }
        }
        i += 1;
        if(i >= n) 
        {
            break;
        }

        {
            p1.new_game();
            p2.new_game();
            var p_win = match(p2, p1);
            if(p_win == 2)
            {
                result.p1_wins += 1;
            }
            if(p_win == 1)
            {
                result.p2_wins += 1;
            }
            if(p_win == 0)
            {
                unreachable;
            }
        }
        i += 1;
    }
    if(result.p2_wins > result.p1_wins)
    {
        result.winner = 2;
    }
    else{
        result.winner = 1; 
    }
    return result;
}

pub fn optimize(p1: *Player, p2: *Player, ref: *Player, n: u32) void {
    var i: u32 = 0;
    while(i < n)
    {
        var result = multiple_matches(p1, p2, 1000);
        if(result.winner == 1)
        {
            p2.* = p1.*;
            p1.mutate(2);
        }
        else
        {
            p1.* = p2.*;
            p2.mutate(2);
        }
        var ref_result = multiple_matches(p1, ref, 1000);
        print("Win rate: {}/{}\n", .{ref_result.p1_wins,1000});
        i += 1;
    }
}

pub fn optimize2(p1: *Player, ref: *Player, n: u32) void {
    var i: u32 = 0;
    var best_player: Player = p1.*;
    var previous_win_rate: u32 = 0;
    while(i < n)
    {
        var result = multiple_matches(p1, ref, 1000);
        if(result.p1_wins > previous_win_rate)
        {
            best_player = p1.*;
            previous_win_rate = result.p1_wins;
            print("High score: {}\n", .{previous_win_rate});
        }
        else
        {
            p1.* = best_player;
        }
        p1.mutate(3);
        print("Round {}, Win rate: {}/{}\n", .{i, result.p1_wins,1000});
        i += 1;
    }
}

pub fn main() !void {
    var p1 = Player.new();
    var ref = Player.new();

    optimize2(&p1, &ref, 3000);

    p1.print_deck();
    ref.print_deck();

    ref = p1;

    optimize2(&p1, &ref, 3000);

    p1.print_deck();
    ref.print_deck();

    ref = p1;

    optimize2(&p1, &ref, 3000);

    p1.print_deck();
    ref.print_deck();

    ref = p1;

    optimize2(&p1, &ref, 3000);

    p1.print_deck();
    ref.print_deck();
}